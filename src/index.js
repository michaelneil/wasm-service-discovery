const wasm = require('./main.rs');

wasm().then(result => {
  const add = result.instance.exports['add'];
  console.log('return value was', add(2, 3));
});