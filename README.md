# WebAssembly Rust

Hoping to turn this into some service discovery module

### Requirements

 - Rust
 - Node

### Getting Started

Install wasm target and wasm garbage collector. The GC reduces the size of the wasm (it does not so runtime GC).

```
rustup target add wasm32-unknown-unknown --toolchain
cargo install --git https://github.com/alexcrichton/wasm-gc
npm install
npm run build
npm start
```

Open your browser to http://localhost:8080/ and check the console. `return value was 5` should be printed.

Check `src/index.js` and change line 5 to add different numbers. Rebuild and reload the browser.